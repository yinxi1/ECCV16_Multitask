\section{Introduction}
Multi-task learning (MTL) aims to learn several tasks {\it simultaneously} to boost the performance of individual tasks due to its ability to discover the correlation among different tasks. 
It has been successfully applied to various computer vision problems, such as face detection~\cite{chen2014joint,zhang2014improving}, face alignment~\cite{zhang2014facial}, face recognition~\cite{ding2015multi}, pedestrian detection~\cite{tian2015pedestrian}, attribute prediction~\cite{abdulnabi2015multi}, and visual tracking~\cite{hong2013tracking}.
For instance, Tian et al.~\cite{tian2015pedestrian} propose to extract patches from non-pedestrian datasets to incorporate hard negative samples for training a robust pedestrian detector, which is an effective way to enlarge the dataset using MTL. 
Li et al.~\cite{li2014heterogeneous} propose a deep network with a pose-joint regressor and a body-part detector for human pose estimation. 

Despite the success of MTL in various vision problems, there is a lack of comprehensive study of MTL for face recognition - an active research topic in the vision community. 
{\it The research gap is especially evident because face recognition can be considered as a multi-task problem}. 
That is, ultimately face recognition should be able to discern the identity variation with other variations in Pose, Illumination, and Expression (PIE), which are essentially the main task of identity recognition and the side tasks of recognizing three individual variations.

However, most existing work treat face recognition as a {\it single} task of learning robust features. 
Recent progress in face recognition is mainly focus on constrainted (a.k.a. in-the-wild) datasets, e.g., Labeled Face in the Wild (LFW)~\cite{huang2007labeled}, IARPA Janus Benchmark A (IJBA)~\cite{klare2015pushing}, and Celebrities in Frontal Profile (CFP)~\cite{senguptafrontal}, where the diverse PIE variations are handled {\it implicitly}. 
Especially LFW becomes the de facto standard database in recent years.
On these databases, Convolutional Neural Network (CNN)-based methods~\cite{schroff2015facenet,taigman2014deepface} also become the de facto standard framework due to their state-of-the-art performance for the face verification task on LFW~\cite{huang2007labeled}. 
%More challenging datasets have been proposed such as IARPA Janus Benchmark A (IJBA) dataset~\cite{klare2015pushing}, which contains  a mixture of images and videos of $500$ subjects with full-pose variations, and Celebrities in Frontal-Profile (CFP) dataset~\cite{senguptafrontal}, which includes extreme pose variations in the wild. 

While face recognition in unconstrained environment made great progress and near perfect performance is reported on LFW, we would like to remind the readers that face recognition in  constrained environment, normally considered as a less challenging counterpart, is in fact far from being resolved.  
For example, 
%In contrast to these unconstrained datasets, 
Multi-PIE~\cite{gross2010multi} is a large constrained dataset including $337$ subjects with a large variation in pose, expression, and illumination. 
In the work of~\cite{xiong2015conditional}, when the test set includes $15$ poses from $99$ subjects, the rank-1 recognition rate is merely $76.89\%$.
In fact, whenever Multi-PIE is used, all prior face recognition work only evaluate on one or two variations, raher than all its variations in PIE and across four sessions.
Considering these facts, we view that {\it there is a clear need to study the constrained face recognition problem}.
Further, due to its diverse variations and annotations in PIE, Multi-PIE is an ideal database for not only constrained face recognition, but also investigating MTL in face recognition.  

\iffalse
It is an ideal dataset to study multi-task learning for face recognition. 
Among these variations, pose has been considered as the most challenging one. 
Therefore, most prior work have been proposed to handle pose variations.
A comprehensive survey on pose-invariant face recognition can be found in~\cite{ding2015comprehensive}, we present a few that utilize MTL.  
For example, Ding et al.~\cite{ding2015multi} propose to transform the features of different poses into a discriminative subspace, and the transformation is learnt jointly for all poses where each pose is considered as one task. 
Yim~\cite{yim2015rotating} propose a deep neural network to rotate a face image at an arbitrary pose while preserving identity, in which the reconstruction of the face is considered as an auxiliary task. 
This multi-task framework has proved to be more effective than single task without appending the reconstruction layer. 
Other variations except pose have been less studied in the literature for face recognition using MTL.  
\fi

To address the aforementioned research needs, this paper aims to improve constrained face recognition by incorporating MTL into the state-of-the-art CNN framework. 
We choose to utilize {\it all} data in Multi-PIE, i.e., faces under the full range of variations in pose, expression, and illumination, as the experimental dataset.
This choice can not only explore the full potential of multi-task CNN, but also demonstrate the challenge of constrained face recognition.
%by explicitly leveraging pose, expression, and illumination annotations in Multi-PIE
To the best of our knowledge, there is no prior work that study the full range of variations in Multi-PIE. 
Our multi-task CNN is motivated by the deep multi-task model for face alignment with the help of facial attributes~\cite{zhang2014facial}.
In their network, the last fully connected layer is shared by multiple tasks to estimate landmark locations as well as facial attributes. 
Considering the scenario when a human being is given an image of a person, he or she will instinctively recognize the identity, pose, expression, and lighting condition at the same time.   
Therefore, we design a similar network structure as in~\cite{zhang2014facial} for face recognition. 
Given an input face image, our framework will jointly extract identity feature and estimate pose, expression and illumination. 
Furthermore, we propose a new technique to direct different routes for face images with different pose in later fully connected layers, which further improve the performance of face recognition. 
This pose-direct MTL can extract generic identity features that are robust to all poses and pose-specific identity features that are robust in a small range of pose. 
These two features compensate each other and improve the performance of face recognition. 
Our experiments have demonstrated that adding side tasks for face recognition is always helpful. 
And the proposed pose-directed MTL is very effective for extracting identity features. 

This work has the following contributions: 
\begin{itemize}
\item We study the problem of face recognition, and pose, illumination, expression estimation as a multi-task problem, which has not been studied in the community, by applying an existing model originally designed for landmark detection. 

\item We propose a new technique to direct different routes for faces with different poses inside the multi-task CNN model to boost face recognition performance. 

\item This is the first work that ever utilize the full set of Multi-PIE, including all poses, expressions, and illuminations for face recognition. 
\end{itemize}







   
 




