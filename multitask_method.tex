\section{The Proposed Approach}
We present the proposed approach in this section. 
First, we apply a deep MTL architecture~\cite{zhang2014facial}, which is originally designed for facial landmark detection, for face recognition where pose, expression, and illumination estimation are considered as three side tasks.
Different from~\cite{zhang2014facial}, we carefully study the impact of sharing different numbers of layers on the face recognition performance. 
Second, we propose a novel task-directed MTL-CNN architecture to tackle pose variations by grouping all poses into five groups and jointly learning identity features for each group. 
During the testing stage, the estimated pose of an input image is used to automatically direct the path in the network by selecting weights from the estimated pose group. 

\subsection{Multi-Task Learning for CNN}
Given a set of $N$ training images and their labels, ${\bf{D}} = \{{\bf{x}}_i, {\bf{y}}_i\}_{i=1}^{N}$, where each label ${\bf{y}}_i = (y_i^d, y_i^p, y_i^e, y_i^m)$ is a four-tuple consists of identity, pose, expression, and illumination labels respectively, we learn a multi-task CNN model such that given a new input image $\bf{x}$, our model is able to extract identity feature and estimate pose, expression, and illumination at the same time.
The identity features extracted from a face image can be used for face identification or verification. 
This design is motivated by the fact that human beings unconsciously perform the above tasks simultaneously when presented a face image. 

\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.7\textwidth]{Figures/overview_1.pdf}
\end{center}
\caption{The multi-task CNN architecture. The top part is the shared common layers. For the bottom part, we study three different network structures where the number of shared fully connected layers are: none in $(a)$, one in $(b)$, and two in $(c)$. Each color is associated with one task. Each of the final output is connected to a softmax loss layer, omitted for simplification, for final estimation.} % This layer is omitted for simplification.}
\label{fig:overview_1}
\end{figure}

%Inspired by the multi-task framework for facial landmark detection~\cite{zhang2014facial}, 
The network structure of our multi-task CNN is shown in Figure~\ref{fig:overview_1}. 
The input is a $100\times100$ gray-scale face image. 
The network is consists of three convolutional layers, three max-pooling layers, and three fully connected layers, where the parameter settings can be found in Figure~\ref{fig:overview_1}.
It has been proved that the weights in early layers acts like edge detectors, thus they are shared for different tasks. %TODO weights or parameters?
For later layers, the network learns more specific feature for high-level tasks. 
One interesting question is that, in the context of multi-task CNN, how many fully connected layers should be shared among different tasks? 
For example, only one fully connect layer is shared in the multi-task network proposed in~\cite{zhang2014improving} for face detection. 
~\cite{zhang2014facial} proposes to share all fully connected layers for facial landmark detection. % TODO: are these two work using similar structure as Fig.1? If not, contrast it, if yes, say it.

We answer this question by designing three different network structures as shown in Figure~\ref{fig:overview_1} (a), (b), (c), where all tasks sharing $0, 1,$ and $2$ fully connected layers, respectively.
Here Figure~\ref{fig:overview_1} (c) has the same structure as~\cite{zhang2014facial}.
Taking the network (a) as an example, the first fully connected layer with size of $1024$ is evenly splitted to four parts each with $256$ to connect to later layers for each task. 
This is equivalent to generate a fully connected layer for each task from the last convolutional layer. 
Therefore, no fully connected layer is shared among these tasks. 
Similarly, one fully connected layer is shared in (b) and two in (c). 

A softmax loss layer is appended to the final output for each classification task. 
The number of output is equal to the number of classes for each task. 
For all layers in our network, rectified linear unit~\cite{nair2010rectified} is used as the activation function. 
And dropout~\cite{hinton2012improving} is applied in the first two fully connected layers. 

Given the training set ${\bf{D}}$, our multi-task CNN learning can be formulated as minimizing the combined loss of all tasks with respect to the network parameters, i.e., 
\begin{equation}
\begin{split}
\underset{{\bf{W}}^{d,p,e,m}} {\mathrm{argmin}} \sum_{i=1}^{N} l^{d}(y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{d})) + 
\alpha \sum_{i=1}^{N} l^{p}(y_i^{p}, f({\bf{x}}_i; {\bf{W}}^{p})) + \\
\beta \sum_{i=1}^{N} l^{e}(y_i^{e}, f({\bf{x}}_i; {\bf{W}}^{e})) +
\gamma \sum_{i=1}^{N} l^{m}(y_i^{m}, f({\bf{x}}_i; {\bf{W}}^{m})) +
\parallel {\bf{W}}^{d,p,e,m} \parallel ^2,
\end{split}
\label{eqn:obj_1}
\end{equation}
where $f({\bf{x}_i}; {\bf{W}})$ is a non-linear mapping function. 
 ${\bf{W}}^d, {\bf{W}}^p, {\bf{W}}^e, {\bf{W}}^m$ are the weights for identity, pose, expression, and illumination estimation respectively, where part of them are shared. 
$\alpha, \beta$, and $\gamma$ are used to control the importance of pose, expression, and illumination classification.  
 The last term is the regularization to penalize the complexity of the weights. 
 $l(\cdot)$ is the cross-entropy loss for each of the classification task,
 \begin{equation}
 l(y_i, f({\bf{x}}_i; {\bf{W}})) = -\mbox{log}(p(y_i \mid {\bf{x}}_i; {\bf{W}})),
 \end{equation}
where the superscript for each task is omitted for clarity. 
$p(\cdot)$ is the softmax function that computes the class posterior probability.
In our network, identity classification is considered as the main task, where the final output is used as the identity feature for face recognition during the testing.
Similar to prior work~\cite{}, we use the cosine distance between two identity features as the similarity metric of two testing face images.


We use stochastic gradient descent to solve the optimization problem of Equation~\ref{eqn:obj_1} through back propagation. 
In our experiments, we have observed that sharing more layers among these tasks increases the performance, as will be presented in Table~\ref{}. 
Therefore, we will build our network structure on top of $(c)$ in the remaining of this paper. 


\subsection{Task-Directed Multi-Task Learning for CNN}
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.99\textwidth]{Figures/overview_2_backup.pdf}
\end{center}
\caption{The proposed task-directed MTL-CNN architecture. The top part is the same as Figure~\ref{fig:overview_1} (c). The bottom-right part is designed to simultaneously extract pose-specific features for each of five pose groups. During the testing stage, the estimated pose group is used to automatically direct the path for an input testing image. Each of the final output is connected to a softmax loss layer, omitted for simplification, for final estimation.}
\label{fig:overview_2}
\end{figure}

In the aforementioned MTL-CNN structure, the CNN weights of $\bf{W}^d$ is responsible to representing a non-linear mapping function to produce the correct identity $y^d_i$ from a face image $\bf{x}_i$, with arbitrary pose, expression and illumination.
This is clearly a very challenging learning problem for $\bf{W}^d$ to achieve such a powerful mapping, considering the diverse PIE variation in $\bf{x}_i$.
In other words, we view this learning challenge in CNN is mostly caused by the high variations encompassed in the data. 
The similar challenge has been encountered in classic pattern recognition work.
For example, in order to handle high variations, ~\cite{} proposed to paritition the training data into groups and learn individual classifiers for each group.
Such a divide-and-conquer scheme could potentially be applied to CNN learning as well, especially considering that in MTL-CNN the side tasks are already serving the role of categorize the training data into multiple groups according to different task (or variation).
Therefore, these side tasks can ``divide'' and data and allow CNN to better ``conquer'' them by learning better mapping functions.

Motivated by this key idea, we propose a novel task-directed MTL-CNN model where the side task can categorize the training data into multiple groups, direct them through different routes in the network structure, and jointly learn the CNN weights for all groups.
In the context of face recognition, since pose variation is considered as the most challenging one among other variations~\cite{xiong2015conditional, zhang2013pose, zhu2014multi}, we use pose as the example to illstrate the pose-directed MTL-CNN model, as shown in Figure~\ref{fig:overview_2}. 
%in face recognition, which has been studied the most in literature~\cite{xiong2015conditional, zhang2013pose, zhu2014multi}.
%Considering the fact that pose estimation is a relatively easy task, we propose a pose-directed CNN model as shown in Figure~\ref{fig:overview_2}. 

This network consists of two parts where the first part is exactly the same as in Figure~\ref{fig:overview_1} $(c)$, which is shown to be the best among all three network structures. 
The second part shares the same convolution, pooling, and the first fully connected layers as the first part. 
The novelty of the second part is to group similar poses to be in the same group to learn pose-dependent identity features. 
Specifically, we split all yaw angles in the range of $[-90^\circ, 90^\circ]$ into five different groups: right profile ($-90^\circ, -75^\circ, -60^\circ$), right half-profile ($-45^\circ, -30^\circ$), frontal ($-15^\circ, 0^\circ, 15^\circ$), left half-profile ($30^\circ, 45^\circ$), and left profile ($60^\circ, 75^\circ, 90^\circ$).  
During training process, we use the ground truth pose labels $y^p_i$ to assign a face image into the correct group. 
Therefore, five different sets of weights, each for one group, are learned simultaneously.  
In the testing stage, we use the estimated pose, which is the output of the pose estimation side task, to select corresponding route to extract pose-specific identity feature. 
%TODO, add 1-2 sentence on HOW this is implemented in Caffe.

Similar to Eqn.~\ref{eqn:obj_1}, the objective of pose-directed MTL-CNN can be formulated as:
\begin{equation}
\begin{split}
\underset{{\bf{W}}^{d0,dg,p,e,m}} {\mathrm{argmin}} \sum_{i=1}^{N} l^{d0}(y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{d0})) + \frac{1}{G}\sum_{g=1}^{G}\sum_{i=1}^{N_g} l^{dg} (y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{dg})) + \\
\alpha \sum_{i=1}^{N} l^{p}(y_i^{p}, f({\bf{x}}_i; {\bf{W}}^{p})) + 
\beta \sum_{i=1}^{N} l^{e}(y_i^{e}, f({\bf{x}}_i; {\bf{W}}^{e})) +
\gamma \sum_{i=1}^{N} l^{m}(y_i^{m}, f({\bf{x}}_i; {\bf{W}}^{m})) 
\end{split}
\label{eqn:obj_2}
\end{equation}
where $G = 5$ is the number of different pose groups, and $N_g$ is the number of training images in $g$-th group. 
Similarly, the cross-entropy loss with the softmax function is used. 
The regularization terms are simply omitted for clarity. 

This network structure aims to learn two types of identity features. 
First, ${\bf{W}}^{d0}$ is the weights used to extract generic identity feature that is robust to all poses. 
Second, $\{{\bf{W}}^{dg}\}_{g=1}^5$ are the weights used to extract pose-specific identity feature that is robust in a small pose range. 
These two types of features are complementary to each other, and can both contribute to face recognition. 
Therefore, given two face images, we use the average cosine distance of these two features as the similarity metric.
%Finally, the rank-$1$ identification accuracy is reported. 













